# Judgels Utilities

You can run this on remote!, no need to ssh

Go to `judgels-utilities/script/`, install requirements.

```shell
pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

Put the `env.yml` of your file to to the same folder

```shell
cp ~/judgels-deployment/env.yml ~/judgels-utilities/script/env.yml
```

All the env is similar, unless:

```
platformhint_superadmin_user: your_superadmin_user
platformhint_superadmin_password: your_superadmin_password
```

Refer to `env_example.yml` for the needed env.



Check `judgels.jophiel_user` table on the database to check whether the batch user has been added.

```shell
mysql -u{user} -p{password} -e "SELECT * FROM judgels.jophiel_user"
```

Try to from local

**Ping Jophiel**

```
curl localhost:9001/api/v2/ping
```

