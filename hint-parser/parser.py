import markdown
def main():
    with open("desc.in", "r") as desc:
        text = desc.read()
        html = markdown.markdown(text)
        text = html.split('\n')
        inside = False
        md = ""
        text = text[1:]
        for line in text:
            if(line.startswith('<p>[Hint ') and line.endswith(']</p>')):
                if (inside):
                    md += "</div>\n</div>"
                inside = True
                line = line[4:-5]
                md += (f'<div class=\"spoiler\">{line}\n<div class=\"spoiler-content\">\n')
            else:
                md += (f'{line}\n')
        if(inside):
            md += "</div>\n</div>"
        html = md
        print(html)
if __name__ == "__main__":
    main()