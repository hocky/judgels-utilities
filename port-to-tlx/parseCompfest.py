import time
import re
import os
import json
import traceback
from zipfile import ZipFile

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import csv

USER_NAME = "hocky"
PASSWORD = "BukanPassword"

def open_csv(filename):
    with open(filename, 'r') as csv_file:
        csv_content = list(csv.reader(csv_file))
        csv_header = csv_content[0]
        csv_content = csv_content[1:]
        return (csv_header, csv_content)

def openJSON(file):
    with open(file) as json_file:
        return json.load(json_file)


def openString(file):
    with open(file) as str_file:
        return str_file.read()

HEADER, SLUGS = open_csv('data.csv')
print(SLUGS)

def rewrite_csv(filename):
    with open(f'{filename}{time.time()}.csv', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(HEADER)
        writer.writerows(SLUGS)

def forceQuit(driver):
    driver.quit()
    exit(0)


def setCheck(driver, checkBox, val):
    if (val == 0):
        if (checkBox.is_selected()): checkBox.click()
    if (val == 1):
        if (not checkBox.is_selected()): checkBox.click()


def makeRepo(driver, slug):
    if(slug[2]): return
    driver.get("https://sandalphon.tlx.toki.id/problems/new")

    slugInput = driver.find_element_by_id("slug")
    slugInput.send_keys(slug[1])

    additionalInput = driver.find_element_by_id("additionalNote")
    info = slug[1].split('-')
    additionalInput.send_keys(f"COMPFEST 13\n{info[2].upper()} - {info[3].title()}")

    languageInput = Select(driver.find_element_by_id("initLanguageCode"))
    languageInput.select_by_value("id-ID")

    time.sleep(2)

    # forceQuit(driver)
    driver.find_element_by_css_selector('button').click()
    time.sleep(2)
    driver.find_element_by_css_selector('button').click()
    time.sleep(2)

    repoID = re.search(r"[/](\d+)[/]?", driver.current_url).group(1)
    slug[2] = str(repoID)
    return

def fillRepo(driver, id, slug):
    currentDir = os.path.join(os.getcwd(), "compfest13")
    repoPath = os.path.join(currentDir, slug)

    gradingPath = os.path.join(repoPath, "grading")
    statementPath = os.path.join(repoPath, "statements")

    # Add languages
    print(f"Adding languages for {slug} at {id}")
    driver.get(f"https://sandalphon.tlx.toki.id/problems/{id}/statements/languages")

    languageList = openJSON(os.path.join(statementPath, 'availableLanguages.txt'))

    for lang in languageList.keys():
        time.sleep(0.5)
        print(f"Trying {lang}")
        if (languageList[lang] == "DISABLED"): continue
        try:
            languageInput = Select(driver.find_element_by_css_selector('[name="langCode"]'))
            languageInput.select_by_value(lang)
            time.sleep(0.5)
            driver.find_element_by_css_selector('button').click()
        except:
            pass

    # Fill statement
    print(f"Filling statements for {slug} at {id}")
    for lang in languageList.keys():
        time.sleep(0.5)
        print(f"Trying {lang}")
        if (languageList[lang] == "DISABLED"): continue
        # try:
        # Change to selected language
        driver.get(f"https://sandalphon.tlx.toki.id/problems/{id}/statements/edit")
        languageInput = Select(driver.find_element_by_css_selector('[name="langCode"]'))
        languageInput.select_by_value(lang)
        driver.find_element_by_css_selector('button').click()
        time.sleep(5)

        # Inserting title
        title = openString(os.path.join(statementPath, lang, "title.txt"))
        print("Inserting Title")
        print(title)
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys(title)
        time.sleep(0.5)

        print("Changing Mode")
        print("Inserting Description")
        desc = openString(os.path.join(statementPath, lang, "text.html"))
        # Select iFrame
        time.sleep(5)
        element = driver.find_element_by_class_name("cke_wysiwyg_frame")
        # Switch to iFrame
        driver.switch_to.frame(element)
        # Query document for the ID that you're looking for
        queryElement = driver.find_element_by_tag_name("body")
        cmd = "arguments[0].innerHTML = {0}".format(json.dumps(desc))
        driver.execute_script(cmd, queryElement)
        driver.switch_to.default_content()

        print(desc)
        buttons = driver.find_elements_by_css_selector('button')
        print(buttons)
        buttons[1].click()
        # except:
        #     pass

    # Upload Media
    print(f"Upload media for {slug} at {id}")
    driver.get(f"https://sandalphon.tlx.toki.id/problems/{id}/statements/media")
    resourceFolder = os.path.join(statementPath, "resources")
    for folderName, subfolders, filenames in os.walk(resourceFolder):
        # print(folderName, subfolders, filenames)
        for filename in filenames:
            if (not filename.startswith('.')):
                print(f'Uploading {filename}')
                # create complete filepath of file in directory
                filePath = os.path.join(folderName, filename)
                # Add file to zip
                driver.find_element_by_id("file").send_keys(filePath)
                driver.find_elements_by_css_selector('button')[0].click()
                time.sleep(2)

    # Set engine
    print(f"Setting engine for {slug} at {id}")
    driver.get(f"https://sandalphon.tlx.toki.id/problems/programming/{id}/grading/engine")
    engineType = openString(os.path.join(gradingPath, "engine.txt"))
    engineInput = Select(driver.find_element_by_id("gradingEngineName"))
    engineInput.select_by_value(engineType)
    time.sleep(2)
    driver.find_elements_by_css_selector('button')[0].click()

    tcNames = []

    packet = 0
    currentPacketSize = 0
    # Zip files into parts
    for folderName, subfolders, filenames in os.walk(os.path.join(gradingPath, 'testdata')):
        for filename in filenames:
            if (filename.endswith('.in') or filename.endswith('.out')):
                print(f'Uploading {filename}')
                filePath = os.path.join(folderName, filename)
                # create complete filepath of file in directory
                print(currentPacketSize)
                if(currentPacketSize + os.stat(filePath).st_size > 10000000):
                    zipObj = ZipFile(os.path.join(gradingPath, 'testdata', f'{packet}.zip'), 'w')
                    for tc in tcNames:
                        zipObj.write(os.path.join(folderName, tc), tc)
                        print(f"Packing {tc} into {packet}.zip")
                    zipObj.close()
                    packet += 1
                    currentPacketSize = 0
                    tcNames = []
                currentPacketSize += os.stat(filePath).st_size
                tcNames.append(filename)
        if(tcNames):
            zipObj = ZipFile(os.path.join(gradingPath, 'testdata', f'{packet}.zip'), 'w')
            for tc in tcNames:
                zipObj.write(os.path.join(folderName, tc), tc)
                print(f"Packing {tc} into {packet}.zip")
            zipObj.close()


    while (1):
        try:
            # Upload Test Data
            print(f"Uploading test data for {slug} at {id}")
            driver.get(f"https://sandalphon.tlx.toki.id/problems/programming/{id}/grading/testdata")
            for part in range(0, 100):
                try:
                    tcPath = os.path.join(gradingPath, "testdata", f"{part}.zip")
                    driver.find_element_by_id("fileZipped").send_keys(tcPath)
                    driver.find_elements_by_css_selector('button')[1].click()
                    time.sleep(5)
                except:
                    break

            # Upload Helpers
            print(f"Uploading helpers for {slug} at {id}")
            driver.get(f"https://sandalphon.tlx.toki.id/problems/programming/{id}/grading/helpers")
            resourceFolder = os.path.join(gradingPath, "helpers")
            for folderName, subfolders, filenames in os.walk(resourceFolder):
                # print(folderName, subfolders, filenames)
                for filename in filenames:
                    if (not filename.startswith('.')):
                        print(f'Uploading {filename}')
                        # create complete filepath of file in directory
                        filePath = os.path.join(folderName, filename)
                        # Add file to zip
                        driver.find_element_by_id("file").send_keys(filePath)
                        driver.find_elements_by_css_selector('button')[0].click()
                        time.sleep(2)

            # Configure tc
            print(f"Configuring TCs for {slug} at {id}")
            driver.get(f"https://sandalphon.tlx.toki.id/problems/programming/{id}/grading/config/tokilib")
            time.sleep(2)

            config = openJSON(os.path.join(gradingPath, "config.json"))

            # Set Custom Scorer and Limits
            if ("customScorer" in config.keys() and config["customScorer"] != None):
                scorer = Select(driver.find_element_by_id("customScorer"))
                scorer.select_by_value(config["customScorer"])

            if ("communicator" in config.keys() and config["communicator"] != None):
                scorer = Select(driver.find_element_by_id("communicator"))
                scorer.select_by_value(config["communicator"])

            try:
                if ("timeLimit" in config.keys() and config["timeLimit"] != None):
                    timeLimit = driver.find_element_by_id("timeLimit")
                    timeLimit.clear()
                    timeLimit.send_keys(config["timeLimit"])
            except:
                pass

            try:
                if ("memoryLimit" in config.keys() and config["memoryLimit"] != None):
                    memoryLimit = driver.find_element_by_id("memoryLimit")
                    memoryLimit.clear()
                    memoryLimit.send_keys(config["memoryLimit"])
            except:
                pass

            for i in range(len(config["subtaskPoints"]) + 100):
                try:
                    pointBox = driver.find_element_by_id(f"subtaskPoints_{i}")
                    pointBox.clear()
                    pointBox.send_keys(config["subtaskPoints"][i])
                except:
                    pass

            curst = 0

            for i in range(50):
                try:
                    setCheck(driver, driver.find_element_by_css_selector(f'[name="sampleTestCaseSubtaskIds[{i}][0]"]'),
                             0)
                except:
                    break
                for j in range(50):
                    try:
                        setCheck(driver,
                                 driver.find_element_by_css_selector(f'[name="sampleTestCaseSubtaskIds[{i}][{j}]"]'), 0)
                    except:
                        break

            for i in range(50):
                try:
                    setCheck(driver, driver.find_element_by_css_selector(f'[name="testGroupSubtaskIds[{i}][0]"]'), 0)
                except:
                    break
                for j in range(50):
                    try:
                        setCheck(driver, driver.find_element_by_css_selector(f'[name="testGroupSubtaskIds[{i}][{j}]"]'),
                                 0)
                    except:
                        break

            # Set test data
            for i in range(len(config["testData"])):
                currentData = config["testData"][i]
                isSample = 0
                for j in range(len(currentData["testCases"])):
                    currentTest = currentData["testCases"][j]
                    for checkID in currentTest["subtaskIds"]:
                        if ("sample" in currentTest["input"]):
                            isSample = 1
                            if (checkID == 0): continue
                            setCheck(driver, driver.find_element_by_css_selector(
                                f'[name="sampleTestCaseSubtaskIds[{j}][{checkID - 1}]"]'), 1)
                        else:
                            setCheck(driver, driver.find_element_by_css_selector(
                                f'[name="testGroupSubtaskIds[{curst}][{checkID - 1}]"]'), 1)
                    if (not isSample): break
                if not isSample: curst += 1

            time.sleep(5)
            driver.find_elements_by_css_selector('button')[1].click()

            break
        except Exception as e:
            # printing stack trace 
            traceback.print_exc()
            continue


def upload():
    # Change to another webdriver if desired (and update CI accordingly).
    options = webdriver.chrome.options.Options()

    # These options are needed for CI with Chromium.
    options.add_argument('--start-maximized')

    path = os.path.dirname(os.path.abspath(__file__))

    options.add_experimental_option("prefs", {
        "download.default_directory": path,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing.enabled": True
    })

    driver = webdriver.Chrome(options=options)
    driver.get("https://sandalphon.tlx.toki.id/problems")

    # Waiting for TLX to fully load
    time.sleep(1)

    # Loggin in user
    usernameInput = driver.find_element_by_id("username")
    passwordInput = driver.find_element_by_id("password")
    usernameInput.send_keys(USER_NAME)
    passwordInput.send_keys(PASSWORD)
    usernameInput.send_keys(Keys.ENTER)

    time.sleep(1)

    for slug in SLUGS:
        print("Trying to do", slug)
        if(slug[3] == '1'): continue
        if("penyisihan" not in slug[1]):
            continue
        print("Trying to do", slug)
        makeRepo(driver, slug)
        fillRepo(driver, slug[2], slug[1])
        slug[3] = '1'
        print("Finish", slug[1])
        time.sleep(10)

    time.sleep(10)
    driver.quit()


def main():
    # content
    upload()
    rewrite_csv('tmp')


if __name__ == "__main__":
    main()
