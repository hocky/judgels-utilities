package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
	"strconv"
)


type Data struct {
	ReceiverSecret string                 `json:"receiverSecret"`
	ContestJID     string                 `json:"contestJid"`
	ContestStyle   string                 `json:"contestStyle"`
	Scoreboards    map[string]interface{} `json:"scoreboards"`
	UpdatedTime    int                    `json:"updatedTime"`
}

type Entry struct {
	Type         string      `json:"type"`
	ContestStyle string      `json:"contestStyle"`
	UpdatedTime  int         `json:"updatedTime"`
	Scoreboard   interface{} `json:"scoreboard"`
}

type DBContests struct {
	Contests  map[string]map[string]string `json:"contests"`
}

var receiverSecret string
var contestSettings map[string]map[string]string

func main() {
	receiverSecret = os.Getenv("RECEIVER_SECRET")
	if receiverSecret == "" {
		panic("RECEIVER_SECRET is not set")
	}
	contestSettings = getContestSettings()
	
	log.Println("Started scoreboard receiver")

	http.HandleFunc("/receive", receive)
	http.HandleFunc("/scoreboard", serveScoreboard)
	http.HandleFunc("/contestant", serveContestant)
	http.ListenAndServe(":9144", nil)
}

func receive(w http.ResponseWriter, r *http.Request) {
	var data Data

	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		log.Println("Received bad scoreboard update")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if data.ReceiverSecret != receiverSecret {
		log.Println("Received scoreboard update with wrong secret")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	log.Printf("Received scoreboard update for contest %s", data.ContestJID)

	for typ, scoreboard := range data.Scoreboards {
		entry := Entry{
			Type:         typ,
			ContestStyle: data.ContestStyle,
			UpdatedTime:  data.UpdatedTime,
			Scoreboard:   scoreboard,
		}

		filename := getScoreboardFilename(data.ContestJID, typ)
		entryBytes, _ := json.MarshalIndent(entry, "", "  ")

		err := ioutil.WriteFile(filename, entryBytes, 0644)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func serveScoreboard(w http.ResponseWriter, r *http.Request) {
	contestJID := r.URL.Query().Get("contestJid")
	contestFrozenTime, _:= strconv.ParseInt(contestSettings[contestJID]["frozenTime"], 10, 64)
	if contestJID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	typ := r.URL.Query().Get("type")
	if typ != "" {
		secret := r.URL.Query().Get("secret")
		if secret != contestSettings[contestJID]["secret"] {
			log.Println("Received scoreboard request with wrong secret")
			w.WriteHeader(http.StatusForbidden)
			return
		}
	}

	if typ != "OFFICIAL" && typ != "FROZEN" {
		typ = "FROZEN"
		if time.Now().Unix() < contestFrozenTime {
			typ = "OFFICIAL"
		}
	}

	filename := getScoreboardFilename(contestJID, "OFFICIAL")
	if typ == "FROZEN" {
		frozenFilename := getScoreboardFilename(contestJID, "FROZEN")
		_, err := os.Stat(frozenFilename)
		if err == nil {
			filename = frozenFilename
		}
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	http.ServeFile(w, r, filename)
}

func serveContestant(w http.ResponseWriter, r *http.Request) {
	contestJID := r.URL.Query().Get("contestJid")
	if contestJID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	filename := getContestantFilename(contestJID)
	_, err := os.Stat(filename)
    if os.IsNotExist(err) {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	http.ServeFile(w, r, filename)
}

func getContestSettings() map[string]map[string]string {
	jsonFile, err := os.Open("var/data/contest-settings.json")
	if err != nil {
        fmt.Println(err)
	}
	
    fmt.Println("Successfully opened contest-settings.json")
    defer jsonFile.Close()
    byteValue, _ := ioutil.ReadAll(jsonFile)
	
	var data DBContests
	json.Unmarshal(byteValue, &data)

	return data.Contests
}

func getContestantFilename(contestJID string) string {
	return fmt.Sprintf("var/data/contestant/%s.json", contestJID)
}

func getScoreboardFilename(contestJID string, typ string) string {
	return fmt.Sprintf("var/data/scoreboard/%s-%s.json", contestJID, typ)
}
