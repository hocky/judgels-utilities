import { SnowflakesAnimationService } from './snowflakes-animation.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as AOS from 'aos';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  title = 'scoreboard-frontend-cf12';

  constructor(private metaService: Meta, public snow: SnowflakesAnimationService) { }

  ngAfterViewInit(): void {

    AOS.init({
      startEvent: 'load',
      once: true
    });

  }

  ngOnInit(): void {

    this.metaService.addTags([
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
      { name: 'theme-color', content: '#ff5e13' },
      { name: 'msapplication-navbutton-color', content: '#ff5e13' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#ff5e13' }
    ]);

  }

}
