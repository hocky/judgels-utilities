import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoiScoreboardComponent } from './ioi-scoreboard.component';

describe('IoiScoreboardComponent', () => {
  let component: IoiScoreboardComponent;
  let fixture: ComponentFixture<IoiScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoiScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoiScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
