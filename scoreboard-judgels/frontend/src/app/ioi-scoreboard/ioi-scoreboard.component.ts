import { SnowflakesAnimationService } from './../snowflakes-animation.service';
import { environment } from './../../environments/environment';
import { ScoreboardService } from './../scoreboard.service';
// import { NavigationBarService } from './../navigation-bar.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-ioi-scoreboard',
  templateUrl: './ioi-scoreboard.component.html',
  styleUrls: ['./ioi-scoreboard.component.css']
})
export class IoiScoreboardComponent implements OnInit, OnDestroy {

  scoreboardData: any;
  contestantsData: any;

  interval: any;
  intervalBetweenUpdates = 30000;

  previousRanking = [];
  rankDiff = [];
  updatedTime: Date;

  title = 'Scoreboard | ' + environment.eventName.toUpperCase();

  upArrow = "/assets/img/up-arrow.png";
  downArrow = "/assets/img/down-arrow.png";
  noDiff = "/assets/img/no-diff.png";

  scoreboardAvailable = true

  // constructor(public nav: NavigationBarService, public snow: SnowflakesAnimationService, private ioiScoreboardService: ScoreboardService, private titleService: Title) { }
  constructor(public snow: SnowflakesAnimationService, private ioiScoreboardService: ScoreboardService, private titleService: Title) { }

  ngOnInit(): void {

    this.titleService.setTitle(this.title);

    // this.nav.show();

    this.ioiScoreboardService.getContestants(environment.ioiParams).subscribe(res => {

      this.contestantsData = res;

    });


    this.ioiScoreboardService.getScoreboard(environment.ioiParams).subscribe(res => {

      this.scoreboardData = res;
      if (this.scoreboardData === null) {
        this.scoreboardAvailable = false;
        // this.nav.show()
      } else {
        this.scoreboardAvailable = true;
        if (this.scoreboardData.type == "FROZEN") {
          this.snow.show();
        } else {
          this.snow.hide();
        }
        for (let i = 0; i < this.scoreboardData.scoreboard.content.entries.length; i++) {

          this.previousRanking.push({
            "contestantJid": this.scoreboardData.scoreboard.content.entries[i].contestantJid,
            "contestantRank": this.scoreboardData.scoreboard.content.entries[i].rank
          });

          this.rankDiff.push({
            "contestantJid": this.scoreboardData.scoreboard.content.entries[i].contestantJid,
            "diff": 0
          });

        }

        this.updatedTime = new Date(this.scoreboardData.updatedTime);
      }

    });

    this.interval = setInterval(() => {
      this.ioiScoreboardService.getScoreboard(environment.ioiParams).subscribe(res => {
        this.scoreboardData = res;
        if (this.scoreboardData === null) {
          this.scoreboardAvailable = false;
        } else {
          this.updatedTime = new Date(this.scoreboardData.updatedTime);
          if (this.scoreboardData.type == "FROZEN") {
            this.snow.show();
          } else {
            this.snow.hide();
          }
          this.updateDiff();
        }
      });
    }, this.intervalBetweenUpdates);

  }

  ngOnDestroy() {

    if (this.interval) {
      clearInterval(this.interval);
    }

  }

  redVal(score: number) {

    if (score === null) return 0;
    if (score <= 25) return (score * 8 + 55);
    if (score < 50) return 255;
    if (score === 100) return 0;

    return 255 - (1 * (score / 2));

  }

  greenVal(score: number) {

    if (score === null) return 0;
    if (score <= 25) return 0;

    return 5.1 * (score / 2);

  }

  getContestantName(Jid: string) {

    return this.contestantsData[Jid].name;

  }

  getContestantSchool(Jid: string) {

    return this.contestantsData[Jid].institutionName;

  }

  getRankDiff(Jid: string) {

    let prevRank = this.previousRanking.filter(function (prev) {
      return prev.contestantJid == Jid;
    })[0].contestantRank;

    let currRank = this.scoreboardData.scoreboard.content.entries.filter(function (curr) {
      return curr.contestantJid == Jid;
    })[0].rank;

    let index = this.previousRanking.map(function (e) {
      return e.contestantJid;
    }).indexOf(Jid);

    this.previousRanking[index] = {
      "contestantJid": Jid,
      "contestantRank": currRank
    };

    return (currRank - prevRank);

  }

  getDiffImage(Jid: string) {

    let diff = this.rankDiff.filter(function (rank) {
      return rank.contestantJid == Jid;
    })[0].diff;

    if (diff > 0) return this.downArrow;
    if (diff < 0) return this.upArrow;

    return this.noDiff;

  }

  getAbsDiff(Jid: string) {

    let diff = this.rankDiff.filter(function (rank) {
      return rank.contestantJid == Jid;
    })[0].diff;

    return Math.abs(diff);

  }

  updateDiff() {

    for (let i = 0; i < this.scoreboardData.scoreboard.content.entries.length; i++) {
      let Jid = this.rankDiff[i].contestantJid;
      this.rankDiff[i].diff = this.getRankDiff(Jid);
    }

  }

}

/* Notes
  Buat ngambil ranking sebelumnya
  (this.previousRanking.filter(function (prev) {
          return prev.contestantJid == "JIDUSERtrocd6vOwPytp6AUml0z";
        })[0].contestantRank)
 */
