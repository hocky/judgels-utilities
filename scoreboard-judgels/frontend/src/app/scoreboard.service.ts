import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScoreboardService {

  constructor(private httpService: HttpClient) { }

  getParamString(param: any) {
    let paramString = "";
    for (let key in param) {
      if (paramString != "" && param[key] != "") paramString += "&&";
      paramString += key + "=" + param[key];
    }
    return paramString;
  }

  getScoreboard(param: any) {
    
    return this.httpService.get(environment.scoreboardAddress + "?" + this.getParamString(param));

  }

  getContestants(param: any) {
  
    return this.httpService.get(environment.contestantsAddress + "?" + this.getParamString(param));
  
  }

}
