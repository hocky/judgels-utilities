export const environment = {
  production: true,
  eventName: 'EVENT NAME',
  scoreboardAddress: 'https://PUBLIC-URL-TO-BACKEND/scoreboard',
  contestantsAddress: 'http://PUBLIC-URL-TO-BACKEND/contestant',
  icpcParams: {
    'contestJid': 'JIDCONTEST',
    'type': '', // "OFFICIAL" or "FROZEN" or empty string
    'secret': '' // Contest secret if type is not empty string
  },
  ioiParams: {
    'contestJid': 'JIDCONTEST',
    'type': '',
    'secret': ''
  }
};
